#!/bin/sh

echo "Iniciando compilación..."

mkdir ../build
cd ../build
qmake ..
make
sudo mv jalculator /usr/bin/

echo "copiando archivos..."

sudo mkdir -p /usr/share/jalculator
sudo cp ../icon.png /usr/share/jalculator/

echo "Creando lanzador"

cat > jalculator.desktop << EOF
[Desktop Entry]
Encoding=UTF-8
Version=1.0
Type=Application
Terminal=false
Exec=jalculator
Name=Jalculator
Categories=Qt;Utility;TextEditor;
Comment=Calculator, logic operations 
Icon=/usr/share/jalculator/icon.png
Keywords=calculator;
EOF

echo "instalando entrada de lanzador..."

sudo desktop-file-install jalculator.desktop

echo "removiendo archivos de instalación..."

rm jalculator.desktop
rm -rf ../build
mkdir ../build

echo "Instalación completada con éxito."

