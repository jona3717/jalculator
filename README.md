# Jalculator

Una ligera y simple calculadora.

### Instalación  
Puedes compilarlo tú mismo o dejar que el script lo haga por ti.

## Compilar manualmente

```
make
```

## Instalar mediante script

Ejecuta el archivo install.sh ubicado en la carpeta scripts desde la terminal, Al instalar pedirá ingresar la contraseña para copiar los archivos al sistema.

``` 
./install
``` 

### Desinstalar

Ejecutar el archivo uninstall.sh ubicado en la carpeta scripts.

```
./uninstall
```

## Construido con 🛠️ 

* [C++](https://isocpp.org/) - El lenguaje utilizado para el desarrollo 
* [Qt5](https://www.qt.io/) - El framework utilizado para la parte gráfica de Jopad 

## Autores ✒️ 

* **Jonathan Córdova** - *Desarrollo y documentación* - [jona3717](https://gitlab.com/jona3717) 

## Licencia 

Este proyecto está bajo la Licencia (GPLv3) - mira el archivo [LICENSE.md](LICENSE.md) para detalles.

## Expresiones de Gratitud 

* Comenta a otros sobre este proyecto 
* Invita una cerveza   al desarrollador.  
* Da las gracias públicamente 邏. 
* etc. 



---

