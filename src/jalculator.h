#ifndef JALCULATOR_H
#define JALCULATOR_H

#include <QWidget>

QT_BEGIN_NAMESPACE
namespace Ui { class Jalculator; }
QT_END_NAMESPACE

class Jalculator : public QWidget
{
    Q_OBJECT

public:
    Jalculator(QWidget *parent = nullptr);
    ~Jalculator();
    void operation(char);

private:
    Ui::Jalculator *ui;
    double firstNum=0, secondNum=0;
    char symbol='!';
    QString results;
    bool replace;
    
    bool eventFilter(QObject *object, QEvent *event);

private slots:
    void on_btn0_clicked();
    void on_btn1_clicked();
    void on_btn2_clicked();
    void on_btn3_clicked();
    void on_btn4_clicked();
    void on_btn5_clicked();
    void on_btn6_clicked();
    void on_btn7_clicked();
    void on_btn8_clicked();
    void on_btn9_clicked();
    void on_btnC_clicked();
    void on_btnAc_clicked();
    void on_btnDot_clicked();
    void on_btnEqual_clicked();
    void on_btnAddition_clicked();
    void on_btnSubstract_clicked();
    void on_btnMultiplication_clicked();
    void on_btnDivision_clicked();
    void on_btnPot_clicked();
    void on_btnPercentage_clicked();
    
};
#endif // JALCULATOR_H
