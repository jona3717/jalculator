#include "jalculator.h"
#include "QIcon"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Jalculator w;
    QPixmap icon("/usr/share/jalculator/icon.png");
    w.setWindowIcon(QIcon(icon));
    w.show();
    return a.exec();
}
