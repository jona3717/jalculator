#include "jalculator.h"
#include "ui_jalculator.h"
#include "math.h"
#include "QKeyEvent"
#include "QLocale"
#include "QDebug"

Jalculator::Jalculator(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Jalculator)
{
    ui->setupUi(this);
    qApp->installEventFilter(this);
    replace = true;
    ui->txtInput->setText("0");
}

Jalculator::~Jalculator()
{
    delete ui;
}

void Jalculator::on_btn0_clicked(){
    QString num = ui->txtInput->text();
    
    if(!replace && num != "0")
        ui->txtInput->setText(num + "0");
    else{
        ui->txtInput->setText("0");
        replace = false;
    }
}

void Jalculator::on_btn1_clicked(){
    QString num = ui->txtInput->text();
    
    if(!replace)
        ui->txtInput->setText(num + "1");
    else{
        ui->txtInput->setText("1");
        replace = false;
    }
}

void Jalculator::on_btn2_clicked(){
    QString num = ui->txtInput->text();
    
    if(!replace)
        ui->txtInput->setText(num + "2");
    else{
        ui->txtInput->setText("2");
        replace = false;
    }
}

void Jalculator::on_btn3_clicked(){
    QString num = ui->txtInput->text();
    
    if(!replace)
        ui->txtInput->setText(num + "3");
    else{
        ui->txtInput->setText("3");
        replace = false;
    }
}

void Jalculator::on_btn4_clicked(){
    QString num = ui->txtInput->text();
    
    if(!replace)
        ui->txtInput->setText(num + "4");
    else{
        ui->txtInput->setText("4");
        replace = false;
    }
}

void Jalculator::on_btn5_clicked(){
    QString num = ui->txtInput->text();
    
    if(!replace)
        ui->txtInput->setText(num + "5");
    else{
        ui->txtInput->setText("5");
        replace = false;
    }
}

void Jalculator::on_btn6_clicked(){
    QString num = ui->txtInput->text();
    
    if(!replace)
        ui->txtInput->setText(num + "6");
    else{
        ui->txtInput->setText("6");
        replace = false;
    }
}

void Jalculator::on_btn7_clicked(){
    QString num = ui->txtInput->text();
    
    if(!replace)
        ui->txtInput->setText(num + "7");
    else{
        ui->txtInput->setText("7");
        replace = false;
    }
}

void Jalculator::on_btn8_clicked(){
    QString num = ui->txtInput->text();
    
    if(!replace)
        ui->txtInput->setText(num + "8");
    else{
        ui->txtInput->setText("8");
        replace = false;
    }
}

void Jalculator::on_btn9_clicked(){
    QString num = ui->txtInput->text();
    
    if(!replace)
        ui->txtInput->setText(num + "9");
    else{
        ui->txtInput->setText("9");
        replace = false;
    }
}

void Jalculator::on_btnC_clicked(){
    ui->txtInput->backspace();
}

void Jalculator::on_btnAc_clicked(){
    symbol = '!';
    firstNum = 0;
    secondNum = 0;
    results = "";
    replace = true;
    ui->txtInput->setText("0");
}

void Jalculator::on_btnDot_clicked(){
    QString num = ui->txtInput->text();
    if(num != "0" || results.size() == 0)
        ui->txtInput->setText(num + ".");
    else{
        ui->txtInput->setText("0.");
        results = "";
    }
}

void Jalculator::on_btnEqual_clicked(){
    QString num = ui->txtInput->text();
    
    if(symbol != '!'){
        secondNum = num.toDouble();
        operation(symbol);
        symbol = '!';
        firstNum = 0;
        secondNum = 0;
        replace = true;
        ui->txtInput->setText(results);
    }
}

void Jalculator::on_btnAddition_clicked(){
    if(symbol != '!'){
        on_btnEqual_clicked();
        firstNum =  ui->txtInput->text().toDouble();
        symbol = '+';
    } else{
        QString num = ui-> txtInput->text();
        firstNum = num.toDouble();
        symbol = '+';
    }
    replace = true;
}

void Jalculator::on_btnSubstract_clicked(){
    if(symbol != '!'){
        on_btnEqual_clicked();
        firstNum =  ui->txtInput->text().toDouble();
        symbol = '-';
    } else{
        QString num = ui-> txtInput->text();
        firstNum = num.toDouble();
        symbol = '-';
    }
    replace = true;
}

void Jalculator::on_btnMultiplication_clicked(){
    if(symbol != '!'){
        on_btnEqual_clicked();
        firstNum =  ui->txtInput->text().toDouble();
        symbol = '*';
    } else{
        QString num = ui-> txtInput->text();
        firstNum = num.toDouble();
        symbol = '*';
    }
    replace = true;
}

void Jalculator::on_btnDivision_clicked(){
    if(symbol != '!'){
        on_btnEqual_clicked();
        firstNum =  ui->txtInput->text().toDouble();
        symbol = '/';
    } else{
        firstNum = ui->txtInput->text().toDouble();
        symbol = '/';
    }
    replace = true;
}

void Jalculator::on_btnPot_clicked(){
    if(symbol != '!'){
        on_btnEqual_clicked();
        firstNum =  ui->txtInput->text().toDouble();
        symbol = '^';
    } else{
        QString num = ui-> txtInput->text();
        firstNum = num.toDouble();
        symbol = '^';
    }
    replace = true;
}

void Jalculator::on_btnPercentage_clicked(){
    if(symbol != '!'){
        on_btnEqual_clicked();
        firstNum =  ui->txtInput->text().toDouble();
        symbol = '%';
    } else{
        QString num = ui-> txtInput->text();
        firstNum = num.toDouble();
        symbol = '%';
    }
    replace = true;
}

void Jalculator::operation(char sym){
    switch(sym){
        case '+': results = QString::number(firstNum + secondNum, 'f', 2); break;
        case '-': results = QString::number(firstNum - secondNum, 'f', 2); break;
        case '*': results = QString::number(firstNum * secondNum, 'f', 2); break;
        case '/':
            if (secondNum == 0){
                results = "No zero Division";
            } else{
                results = QString::number(firstNum / secondNum, 'f', 2);
            }
            break;
        case '^': results = QString::number(pow(firstNum, secondNum), 'f', 2); break;
        case '%': results = QString::number(firstNum / 100 * secondNum, 'f', 2); break;
        default: break;
    }
}

bool Jalculator::eventFilter(QObject* object, QEvent* event){
    
    if(object == this && event->type() == QEvent::KeyPress){
        QKeyEvent *key = static_cast<QKeyEvent *>(event);
        if(key->key() == Qt::Key_Escape){
            ui->btnAc->animateClick(200);
        }
        if(key->key() == Qt::Key_Backspace){
            ui->btnC->animateClick(200);
        }
        if(key->key() == Qt::Key_Return){
            ui->btnEqual->animateClick(200);
        }
        if(key->key() == Qt::Key_Plus){
            ui->btnAddition->animateClick(200);
        }
        if(key->key() == Qt::Key_Asterisk){
            ui->btnMultiplication->animateClick(200);
        }
        if(key->key() == Qt::Key_Slash){
            ui->btnDivision->animateClick(200);
        }
        if(key->key() == Qt::Key_Percent){
            ui->btnPercentage->animateClick(200);
        }
        if(key->key() == Qt::Key_AsciiCircum){
            ui->btnPot->animateClick(200);
        }
        if(key->key() == Qt::Key_Ampersand){
            ui->btnPot->animateClick(200);
        }
        if(key->key() == 46){
            ui->btnDot->animateClick(200);
        }
        if(key->key() == 45){
            ui->btnSubstract->animateClick(200);
        }
        if(key->key() == Qt::Key_0){
            ui->btn0->animateClick(200);
        }
        if(key->key() == Qt::Key_1){
            ui->btn1->animateClick(200);
        }
        if(key->key() == Qt::Key_2){
            ui->btn2->animateClick(200);
        }
        if(key->key() == Qt::Key_3){
            ui->btn3->animateClick(200);
        }
        if(key->key() == Qt::Key_4){
            ui->btn4->animateClick(200);
        }
        if(key->key() == Qt::Key_5){
            ui->btn5->animateClick(200);
        }
        if(key->key() == Qt::Key_6){
            ui->btn6->animateClick(200);
        }
        if(key->key() == Qt::Key_7){
            ui->btn7->animateClick(200);
        }
        if(key->key() == Qt::Key_8){
            ui->btn8->animateClick(200);
        }
        if(key->key() == Qt::Key_9){
            ui->btn9->animateClick(200);
        }
    }
    return QObject::eventFilter(object, event);
}
